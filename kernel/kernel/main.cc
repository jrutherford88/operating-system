#include <asmutils.h>
#include <terminal.h>

namespace {
  uint8_t kPIC1Port = 0x20;
  uint8_t kPIC2Port = 0xA0;
  uint8_t kICW1 = 0x11;
  uint8_t kICW4 = 0x01;
}

static void init_pic(uint8_t pic1, uint8_t pic2)
{
  asmutils::out_byte(kPIC1Port, kICW1);
  asmutils::out_byte(kPIC2Port, kICW1);

  asmutils::out_byte(kPIC1Port + 1, pic1); // remap irq0-7
  asmutils::out_byte(kPIC2Port + 1, pic2); // remap irq8-15

  // Set irq2 to be connection to slave pic
  asmutils::out_byte(kPIC1Port + 1, 4);
  asmutils::out_byte(kPIC2Port + 1, 2);

  asmutils::out_byte(kPIC1Port + 1, kICW4);
  asmutils::out_byte(kPIC2Port + 1, kICW4);
}

int main(void) {
  init_pic(0x20, 0x28);

  Terminal& term = Terminal::GetTerm();
  term.ClearScreen();
  term.print("Hello world! This is the %d day of %s!", 18, "December");
  while(1);
  return 0;
}

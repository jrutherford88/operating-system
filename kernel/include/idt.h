#ifndef _IDT_H_
#define _IDT_H_

#include <cstdint>

class IDT
{
  public:
    static void InitPic(uint8_t pic1, uint8_t pic2);
    static void InitIdt();
    IDT() = delete;


  private:
    struct idt_entry_t {
      uint16_t offset_low16_;
      uint16_t selector_;
      uint8_t ist_;
      uint8_t type_attr_;
      uint16_t offset_mid16_;
      uint32_t offset_high32_;
      uint32_t zero_;
    };

    idt_entry_t idt_[128];
};

#endif  // _IDT_H_
